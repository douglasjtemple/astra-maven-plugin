package astra;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class AbstractCmd {
	private BufferedReader inputReader;
	private BufferedReader errorReader;
	protected File baseDir;
	
	public AbstractCmd(File baseDir) {
		this.baseDir = baseDir;
	}
	
	public void execute() throws IOException, InterruptedException {
		String[] envp = new String[] { "PATH="+System.getenv("PATH")};
		Process process = Runtime.getRuntime().exec(getCommand(), envp, baseDir);
		inputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
		errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		displayStream(inputReader);
		displayStream(errorReader);
		if (process.waitFor() > 0) {
			System.exit(1);
		}
	}
	
	public BufferedReader getInputReader() {
		return inputReader;
	}
	
	public BufferedReader getErrorReader() {
		return errorReader;
	}
	
	public String[] getCommand() {
		return new String[] {"skip"};
	}
	
	private void displayStream(final BufferedReader inputReader) {
		new Thread() {
			public void run() {
				try {
					String line = null;
					while ((line = inputReader.readLine()) != null)
						System.out.println(line);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();
	}

}
