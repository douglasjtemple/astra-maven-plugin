package astra;

import java.io.File;
import java.util.List;

public class ASTRARunCmd extends AbstractCompilerCmd {
	private String mainClass;
	private String mainName;
    // private String arguments;
    
	public ASTRARunCmd(File baseDir, String mainClass, String mainName, List<String> classpath) {
		super(baseDir, classpath);
		this.mainClass = mainClass;
        this.mainName = mainName;
//        this.arguments = arguments;
	}
	
	public String[] getCommand() {
		return new String[] { "java", "-cp", getClasspath(), "-Dastra.name="+mainName, mainClass};
	}
}
