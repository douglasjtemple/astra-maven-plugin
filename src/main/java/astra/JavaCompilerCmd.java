package astra;

import java.io.File;
import java.util.List;

public class JavaCompilerCmd extends AbstractCompilerCmd {
	private List<String> files;
	private String target;
	
	public JavaCompilerCmd(File baseDir, String target, List<String> files, List<String> classpath) {
		super(baseDir, classpath);
		this.target =target;
		this.files = files;
		
		// System.out.println("BaseDir: " + baseDir.getAbsolutePath());
		// System.out.println("Classpath: " + getClasspath());
	}
	
	public String[] getCommand() {
		String[] cmd = new String[files.size()+5];
		cmd[0]="javac";
		cmd[1]="-cp";
		cmd[2]=getClasspath();
		cmd[3]="-d";
		cmd[4]=target;
		for (int i=0;i<files.size(); i++) {
			cmd[i+5]=files.get(i);
		}
		return cmd;
	}

	protected String getClasspath() {
		StringBuffer buf = new StringBuffer();
		char delim = ':';
		if (System.getProperty("os.name").startsWith("Windows")) delim = ';';
		buf.append(".");
		for (String jar : classpath) {
			buf.append(delim).append(jar);
		}
		return buf.toString();
	}
	
}
