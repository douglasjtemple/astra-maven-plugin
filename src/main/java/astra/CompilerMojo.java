package astra;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;

@Mojo( name = "compile", defaultPhase = LifecyclePhase.COMPILE, threadSafe = true, requiresDependencyResolution = ResolutionScope.COMPILE)
public class CompilerMojo extends AbstractMojo {

	@Parameter(defaultValue = "${project}", required = true, readonly = true)
	private MavenProject project;

	@Parameter(defaultValue = "${session}", readonly = true)
	private MavenSession session;

	@Parameter(defaultValue = "src/main/astra", readonly = true)
	private String source;

	@Parameter(defaultValue = "target/gen/java", readonly = true)
	private String target;

	public void execute() throws MojoExecutionException, MojoFailureException {
		try {
            // Get the classpath...
            List<String> cp = project.getCompileClasspathElements();
            cp.add(project.getBuild().getOutputDirectory());

            // Compile the ASTRA code
			new ASTRACompileCmd(project.getBasedir(), source, target,cp).execute();
			
            // Check if the output folder has been created (default is target/classes)
            File outputDirectory = new File (project.getBuild().getOutputDirectory());
            if (!outputDirectory.exists()) {
                outputDirectory.mkdirs();
            }
            
			List<String> files = new ArrayList<String>();
			recursiveAdd(new File(project.getBasedir(), target), files, "");
			
            // Compile the generated Java files
			new JavaCompilerCmd(new File(project.getBasedir(), target), project.getBuild().getOutputDirectory(), files, cp).execute();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (DependencyResolutionRequiredException e) {
			e.printStackTrace();
		}
	}

	private void recursiveAdd(File source, List<String> commands, String path) {
//		getLog().info("EXPLORING: " + source.getAbsolutePath() + " / " + path);
		for (File file : source.listFiles()) {
			if(file.isFile()) {
				if (file.getName().endsWith(".java")) {
//					getLog().info("ADDING: " + path+(path==""?"":"/")+file.getName());
					commands.add(path+(path==""?"":"/")+file.getName());
//				} else {
//					getLog().info("IGNORING: " + path+(path==""?"":"/")+file.getName());
				}
			} else if (file.isDirectory()) {
				recursiveAdd(file, commands, path+(path==""?"":"/")+file.getName());
			}
		}
	}
}
